use log::warn;
use std::fmt;
use std::fs;
use std::io::{Error, ErrorKind, Result};
use std::str::FromStr;
pub struct Config {
    pub battery_low: usize,
    pub battery_high: usize,
    pub device: String,
    pub relay_id: u8,
    pub inverted_signal: bool,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            battery_low: 20,
            battery_high: 97,
            relay_id: 1,
            inverted_signal: false,
            device: String::from("/dev/usbrelay_BATCH"),
        }
    }
}

impl fmt::Display for Config {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Battery low: {}%, ", self.battery_low)?;
        write!(f, "Battery high: {}%, ", self.battery_high)?;
        write!(f, "Relay ID: {}, ", self.relay_id)?;
        write!(f, "Inverted signal: {}, ", self.inverted_signal)?;
        write!(f, "Default device: {}", self.device)
    }
}

impl FromStr for Config {
    type Err = Error;
    fn from_str(s: &str) -> Result<Self> {
        let mut conf = Config::default();
        for line in s.lines() {
            let kv: Vec<&str> = line.split('=').collect();
            if kv.len() != 2 {
                continue;
            }
            let key = kv[0].trim();
            let val = kv[1].trim();
            match key {
                "battery_low" => {
                    conf.battery_low = val.parse().unwrap_or_else(|e| {
                        warn!("Could not parse battery_low value: '{}' cause: {}. Falling back to default.", &val, e);
                        conf.battery_low
                    });
                }
                "battery_high" => {
                    conf.battery_high = val.parse().unwrap_or_else(|e| {
                        warn!(
                            "Could not parse battery_high value: '{}' cause: {}. Falling back to default.",
                            &val, e
                        );
                        conf.battery_high
                    });
                }
                "inverted_signal" => {
                    let val = val.to_lowercase();
                    conf.inverted_signal = match val.as_str() {
                        "true" => true,
                        "false" => false,
                        _ => {
                            warn!("Could not parse inverted_signal value: '{}'. Must be true or false, Falling back to false.", val);
                            false
                        }
                    }
                }
                "device" => {
                    conf.device = val.into();
                }
                "relay_id" => {
                    conf.relay_id = val.parse().unwrap_or_else(|e| {
                        warn!("Could not parse battery_high value: '{}' cause: {}. Falling back to default.",
                            &val, e);
                        conf.relay_id
                    });
                }
                _ => {
                    log::warn!("Ignored: {}", key);
                }
            }
        }
        Ok(conf)
    }
}

impl Config {
    pub fn load() -> Result<Self> {
        let mut config_file = dirs::config_dir()
            .ok_or_else(|| Error::new(ErrorKind::Other, "Could not find config_directory"))?;
        config_file.push("batchecker");
        let _ = fs::create_dir(&config_file);
        config_file.push("config");
        match fs::read_to_string(config_file) {
            Ok(conf) => Ok(Config::from_str(&conf)?),
            Err(e) => {
                log::warn!("Could not read config file cause: {:?}", e);
                Ok(Config::default())
            }
        }
    }
}
