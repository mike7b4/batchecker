use std::io::Result;
use std::io::{Read, Write};
use std::os::unix::net::{UnixListener, UnixStream};
use std::thread;
const BAT_SOCKET: &str = "/tmp/batchecker.sock";
pub struct Server {
    listener: UnixListener,
}

fn handle_client(charg_state: bool, bat_lev: usize, mut stream: UnixStream) {
    let format_status = || {
        format!(
            r###"╭─────────────┬───────╮
│Charge state │ {charg_state} │ 
│Battery      │ {bat_lev}%   │
╰─────────────┴───────╯"###
        )
    };
    stream.write_all(format_status().as_bytes()).ok();
}

impl Server {
    pub fn new() -> Result<Self> {
        let listener = UnixListener::bind(BAT_SOCKET)?;
        listener
            .set_nonblocking(true)
            .unwrap_or_else(|e| log::warn!("Could not set non blocking cause: '{}'", e));
        Ok(Self { listener })
    }

    pub fn process(&mut self, charger_state: bool, bat_level: usize) -> Result<()> {
        for stream in self.listener.incoming() {
            match stream {
                Ok(stream) => {
                    thread::spawn(move || handle_client(charger_state, bat_level, stream));
                }
                Err(_) => {
                    break;
                }
            }
        }
        Ok(())
    }
}

impl Drop for Server {
    fn drop(&mut self) {
        std::fs::remove_file(BAT_SOCKET).ok();
    }
}

pub struct Client {
    inner: UnixStream,
}

impl Client {
    pub fn new() -> Result<Self> {
        let inner = UnixStream::connect(BAT_SOCKET)?;
        inner
            .set_read_timeout(Some(std::time::Duration::from_millis(200)))
            .ok();
        Ok(Self { inner })
    }

    pub fn run(&mut self) -> Result<()> {
        let mut input = String::new();
        self.inner.read_to_string(&mut input).ok();
        if !input.is_empty() {
            print!("{}", input);
        }
        Ok(())
    }
}
