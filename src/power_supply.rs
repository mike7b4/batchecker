use std::fmt;
use std::fs;
#[derive(Default)]
pub struct PowerSupply {
    pub level: usize,
    pub is_charging: bool,
}

impl PowerSupply {
    pub fn update(&mut self) {
        let v: String = fs::read_to_string("/sys/class/power_supply/BAT0/capacity")
            .unwrap_or_else(|_| "0".into());
        let v = v.trim();
        self.level = v.parse().unwrap_or(0);

        let v: String = fs::read_to_string("/sys/class/power_supply/BAT0/status")
            .unwrap_or_else(|_| "0".into());
        let v = v.trim();
        self.is_charging = v.contains("Charging");
    }
}

impl fmt::Display for PowerSupply {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Capacity: {}%", self.level).ok();
        write!(
            f,
            "Battery is {}charging.",
            if self.is_charging { "" } else { "not " }
        )
    }
}
