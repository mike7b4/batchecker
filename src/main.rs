mod config;
mod hid;
mod ipc;
mod power_supply;
mod sound;
use crate::power_supply::PowerSupply;
use crate::sound::Sound;
use config::Config;
use libusbrelay::UsbRelay;
use log::{error, info};
use std::env;
use std::io::Result;
use std::io::{Error, ErrorKind};
use std::path::PathBuf;
use std::sync::{
    atomic::{AtomicBool, Ordering},
    Arc,
};
use std::thread;
use std::time::Duration;

fn main() -> Result<()> {
    simple_logger::SimpleLogger::new()
        .with_level(log::LevelFilter::Warn)
        .init()
        .unwrap();
    let name = PathBuf::from(env::args().next().unwrap_or_default());
    let name = name.file_stem().unwrap().to_str().unwrap();
    if name == "batchecker" {
        daemon()
    } else if name == "batchecker-cli" {
        cli()
    } else {
        log::error!("Not supported");
        std::process::exit(127);
    }
}

fn cli() -> Result<()> {
    let mut ipc = ipc::Client::new()?;
    ipc.run()
}

fn daemon() -> Result<()> {
    let terminate = Arc::new(AtomicBool::new(false));
    signal_hook::flag::register(signal_hook::consts::SIGTERM, terminate.clone())
        .expect("Setup signal SIGTERM");
    signal_hook::flag::register(signal_hook::consts::SIGINT, terminate.clone())
        .expect("Setup signal SIGINT");
    let config = Config::load()?;
    info!("Configuration: {}", config);
    let mut relay = UsbRelay::from_path(&config.device)
        .map_err(|e| error!("UsbRelay failed cause: {}", e))
        .ok();
    let mut bat = PowerSupply::default();
    let mut old_bat_level = 0;
    let mut old_charger_state = config.inverted_signal
        ^ relay
            .as_ref()
            .map(|s| {
                s.state(config.relay_id).unwrap_or_else(|e| {
                    error!("USB Relay read failed {}", e);
                    false
                })
            })
            .unwrap_or(false);
    if let Some(r) = &relay {
        info!("Charge state is: {}", old_charger_state);
        info!("{}", r.info()?.to_string().replace('\n', " "));
    }

    let mut listener = ipc::Server::new().map_err(|e| {
        Error::new(
            ErrorKind::Other,
            format!("Could not setup IPC Server socket cause: {}", e),
        )
    })?;
    Sound::play();
    let mut ticks = 0;
    while !terminate.load(Ordering::Relaxed) {
        let new = if let Some(r) = relay.as_ref() {
            r.state(config.relay_id).unwrap_or_else(|e| {
                error!("USB Relay read failed {}", e);
                relay = None;
                false
            })
        } else {
            // No relay active
            false
        };

        let charger_state = config.inverted_signal ^ new;
        if (ticks % 15000) == 0 {
            bat.update();
            if charger_state != old_charger_state {
                info!(
                    "External charger {}activated",
                    if charger_state { "" } else { "de" }
                );
                old_charger_state = charger_state;
            }

            if old_bat_level != bat.level {
                info!("{}", bat);
                old_bat_level = bat.level;
            }

            if bat.level >= config.battery_high && bat.is_charging {
                if let Some(relay) = relay.as_mut() {
                    relay
                        .set(config.relay_id, config.inverted_signal ^ false)
                        .unwrap_or_else(|e| {
                            error!("Could not execute Charge Off. cause of: {:?}", e);
                        })
                }
            } else if bat.level <= config.battery_low && !bat.is_charging {
                if let Some(relay) = relay.as_mut() {
                    relay
                        .set(config.relay_id, config.inverted_signal ^ true)
                        .unwrap_or_else(|e| {
                            error!("Could not execute Charge On. cause of: {:?}", e);
                        })
                }
                Sound::play();
            }
        }

        listener.process(bat.is_charging, bat.level).ok();
        thread::sleep(Duration::from_millis(100));
        ticks += 100;
    }
    Ok(())
}
