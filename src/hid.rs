use log::error;
use nix::{convert_ioctl_res, ioctl_readwrite, request_code_read};
use std::fs::File;
use std::io::{Read, Write};
use std::os::unix::io::{AsRawFd, FromRawFd};
macro_rules! ioctl_read_ptr {
    ($(#[$attr:meta])* $name:ident, $ioty:expr, $nr:expr, $ty:ty) => (
        $(#[$attr])*
        /// # Safety
        /// ioctl call need unsafe calls to C
        pub unsafe fn $name(fd: nix::libc::c_int,
                            data: *const $ty)
                            -> nix::Result<nix::libc::c_int> {
            convert_ioctl_res!(nix::libc::ioctl(fd, request_code_read!($ioty, $nr, ::std::mem::size_of::<$ty>()) as nix::sys::ioctl::ioctl_num_type, data))
        }
    )
}

ioctl_readwrite!(hid_send_feature_report, b'H', 0x06, *const u8);
ioctl_readwrite!(hid_get_feature_report, b'H', 0x07, *mut u8);
ioctl_read_ptr!(hid_cgr_desc_size, b'H', 0x01, i32);
ioctl_read_ptr!(hid_cgr_desc, b'H', 0x02, CHidReportDescriptor);

const HID_MAX_DESCRIPTOR_SIZE: usize = 4096;
#[repr(C)]
pub struct CHidReportDescriptor {
    size: u32,
    value: [u8; HID_MAX_DESCRIPTOR_SIZE],
}

pub struct Hid {
    handle: File,
    #[allow(dead_code)]
    report_descriptor: Vec<u8>,
}

impl Hid {
    #[allow(dead_code)]
    pub fn from_path<P: AsRef<std::path::Path>>(p: P) -> std::io::Result<Self> {
        let fd = nix::fcntl::open(
            p.as_ref(),
            nix::fcntl::OFlag::O_RDWR,
            nix::sys::stat::Mode::empty(),
        )
        .map_err(|_| std::io::Error::last_os_error())?;

        let handle: File = unsafe {
            // Safety: no access to the file after fcntl::open called
            // and it return on fail, so the file descriptor should be valid here.
            File::from_raw_fd(fd)
        };
        let mut desc_size = Box::new(0);
        let ptr = Box::into_raw(desc_size);
        unsafe {
            // Safety: Kernel just borrows the raw pointer
            let _ = hid_cgr_desc_size(handle.as_raw_fd(), ptr);
            // so safe to give it back...
            desc_size = Box::from_raw(ptr);
        };

        let mut report_descriptor = Vec::new();
        if *desc_size > 0 {
            let mut report = Box::new(CHidReportDescriptor {
                size: *desc_size as u32,
                value: [0; 4096],
            });

            let ptr = Box::into_raw(report);
            let res = unsafe {
                // Safety: Kernel just borrows the raw pointer
                let res = hid_cgr_desc(handle.as_raw_fd(), ptr);
                // so safe to give it back...
                report = Box::from_raw(ptr);
                res
            };
            if res.is_ok() {
                report_descriptor.extend(&report.value[0..report.size as usize]);
            } else {
                error!("Error read report descriptors cause: {:?}", res);
            }
        }

        Ok(Self {
            handle,
            report_descriptor,
        })
    }

    #[allow(dead_code)]
    pub fn send_feature_report(&self, bytes: &[u8]) -> Result<u32, std::io::Error> {
        let res = unsafe {
            hid_send_feature_report(self.handle.as_raw_fd(), bytes.as_ptr() as *mut *const u8)
        };
        let res = match res {
            Ok(res) => res as u32,
            Err(res) => {
                error!("Send error cause {:?}", res);
                0
            }
        };
        Ok(res)
    }

    #[allow(dead_code)]
    pub fn get_feature_report(&self, bytes: &[u8]) -> Result<u32, std::io::Error> {
        let res = unsafe {
            hid_get_feature_report(self.handle.as_raw_fd(), bytes.as_ptr() as *mut *mut u8)
        };
        let res = match res {
            Ok(res) => res as u32,
            Err(res) => {
                error!("Read error cause {:?}", res);
                0
            }
        };
        Ok(res)
    }
}

impl Write for Hid {
    fn write(&mut self, bytes: &[u8]) -> Result<usize, std::io::Error> {
        self.handle.write(bytes)
    }

    fn flush(&mut self) -> std::io::Result<()> {
        Ok(())
    }
}

impl Read for Hid {
    fn read(&mut self, bytes: &mut [u8]) -> Result<usize, std::io::Error> {
        self.handle.read(bytes)
    }
}
