use log::error;
use std::process::Command;
const DEFAULT_SOUND_FILE: &str = "/usr/share/sounds/Oxygen-Sys-Warning.ogg";

pub struct Sound {
    sound_file: std::path::PathBuf,
}

impl Default for Sound {
    fn default() -> Self {
        Self {
            sound_file: std::path::PathBuf::from(DEFAULT_SOUND_FILE),
        }
    }
}

impl Sound {
    pub fn play() {
        let sound = Sound::default();
        match Command::new("pw-play")
            .env("DBUS_SESSION_BUS_ADDRESS", "")
            .arg(sound.sound_file)
            .spawn()
        {
            Ok(mut c) => {
                c.wait().ok();
            }
            Err(e) => {
                error!("Could not spawn play cause: '{:?}'", e);
            }
        };
    }
}
